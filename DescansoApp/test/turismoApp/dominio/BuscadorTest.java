package turismoApp.dominio;

import DescansoApp.dominio.ComercioActividad;
import DescansoApp.dominio.Ciudad;
import DescansoApp.dominio.Sistema;
import DescansoApp.herramientas.Buscador;
import DescansoApp.herramientas.TipoCA;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import org.junit.Test;
import static org.junit.Assert.*;

public class BuscadorTest {
    @Test
    public void testBuscarTodas() {
        System.out.println("Buscar todas");
        Sistema instance = new Sistema();
        
        Ciudad ciudad = new Ciudad("Mdeo", "");
        instance.agregarCiudad(ciudad);
        
        Buscador bus = new Buscador(instance.getListaCiudades());
        ArrayList<DescansoApp.dominio.Ciudad> resultados = bus.buscar("*");
        
        assertEquals(resultados.size(),1);
    }
    
    @Test
    public void testBuscarCiudadNombre() {
        System.out.println("Buscar ciudad por nombre");
        Sistema instance = new Sistema();
        
        Ciudad ciudad = new Ciudad("Mdeo", "");
        instance.agregarCiudad(ciudad);
        
        Buscador bus = new Buscador(instance.getListaCiudades());
        ArrayList<DescansoApp.dominio.Ciudad> resultados = bus.buscar("Mdeo");
        
        assertEquals(resultados.size(),1);
    }
    
    @Test
    public void testBuscarCiudadDescrip() {
        System.out.println("Buscar ciudad por descrip");
        Sistema instance = new Sistema();
        
        Ciudad ciudad = new Ciudad("Mdeo", "");
        ciudad.setDescripcion("capital");
        instance.agregarCiudad(ciudad);
        
        Buscador bus = new Buscador(instance.getListaCiudades());
        ArrayList<DescansoApp.dominio.Ciudad> resultados = bus.buscar("capital");
        
        assertEquals(resultados.size(),1);
    }
    
    @Test
    public void testBuscarCiudadInfo() {
        System.out.println("Buscar ciudad por info");
        Sistema instance = new Sistema();
        
        Ciudad ciudad = new Ciudad("Mdeo", "");
        ciudad.setInfoGral("capital");
        instance.agregarCiudad(ciudad);
        
        Buscador bus = new Buscador(instance.getListaCiudades());
        ArrayList<DescansoApp.dominio.Ciudad> resultados = bus.buscar("capital");
        
        assertEquals(resultados.size(),1);
    }
}

