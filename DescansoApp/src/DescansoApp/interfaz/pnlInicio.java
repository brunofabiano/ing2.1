package DescansoApp.interfaz;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import DescansoApp.dominio.Sistema;

public class pnlInicio extends javax.swing.JPanel {

    private final Sistema modelo;
    private final JFrame padre;

    public pnlInicio(Sistema unModelo, JFrame miPadre) {
        initComponents();
        btnAdministracion.setBackground(java.awt.Color.cyan);
        btnMisViajes.setBackground(java.awt.Color.cyan);
        btnNuevoViaje.setBackground(java.awt.Color.cyan);
        modelo = unModelo;
        padre = miPadre;
        this.setMinimumSize(new java.awt.Dimension(800, 600)); 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblBuscar = new javax.swing.JLabel();
        txtBuscador = new javax.swing.JTextField();
        lblBuscador = new javax.swing.JLabel();
        btnAdministracion = new javax.swing.JButton();
        btnNuevoViaje = new javax.swing.JButton();
        btnMisViajes = new javax.swing.JButton();
        lblImagen = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(840, 500));
        setLayout(null);

        lblBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/Lupa.png"))); // NOI18N
        lblBuscar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblBuscarMouseClicked(evt);
            }
        });
        add(lblBuscar);
        lblBuscar.setBounds(770, 420, 20, 20);

        txtBuscador.setFont(new java.awt.Font("Arial", 0, 11)); // NOI18N
        txtBuscador.setBorder(null);
        txtBuscador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscadorKeyReleased(evt);
            }
        });
        add(txtBuscador);
        txtBuscador.setBounds(420, 420, 340, 22);

        lblBuscador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/Buscador.png"))); // NOI18N
        add(lblBuscador);
        lblBuscador.setBounds(380, 320, 440, 160);

        btnAdministracion.setBackground(new java.awt.Color(0, 204, 153));
        btnAdministracion.setText("Administración");
        btnAdministracion.setMaximumSize(new java.awt.Dimension(107, 23));
        btnAdministracion.setMinimumSize(new java.awt.Dimension(107, 23));
        btnAdministracion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdministracionActionPerformed(evt);
            }
        });
        add(btnAdministracion);
        btnAdministracion.setBounds(40, 40, 130, 60);

        btnNuevoViaje.setBackground(new java.awt.Color(0, 204, 153));
        btnNuevoViaje.setText("Nuevo Viaje");
        btnNuevoViaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoViajeActionPerformed(evt);
            }
        });
        add(btnNuevoViaje);
        btnNuevoViaje.setBounds(40, 110, 130, 60);

        btnMisViajes.setBackground(new java.awt.Color(0, 204, 153));
        btnMisViajes.setText("Mis Viajes");
        btnMisViajes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMisViajesActionPerformed(evt);
            }
        });
        add(btnMisViajes);
        btnMisViajes.setBounds(40, 180, 130, 60);

        lblImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/FondoInicio.png"))); // NOI18N
        lblImagen.setPreferredSize(new java.awt.Dimension(800, 450));
        lblImagen.setVerifyInputWhenFocusTarget(false);
        add(lblImagen);
        lblImagen.setBounds(0, 0, 840, 500);
    }// </editor-fold>//GEN-END:initComponents

    
    private void lblBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBuscarMouseClicked
        buscar();
    }//GEN-LAST:event_lblBuscarMouseClicked

    private void txtBuscadorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscadorKeyReleased
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            buscar();
        }
    }//GEN-LAST:event_txtBuscadorKeyReleased

    private void btnAdministracionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdministracionActionPerformed
        padre.remove(this);
        padre.add(new pnlAdmin(modelo, padre), BorderLayout.WEST);
        padre.pack();
    }//GEN-LAST:event_btnAdministracionActionPerformed

    private void btnNuevoViajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoViajeActionPerformed
        // TODO add your handling code here:
        PopUp p= new PopUp();
        p.add(new pnlNuevoViaje(modelo, p, null));
        p.pack();
        p.setLocationRelativeTo(null);
        p.setVisible(true);
    }//GEN-LAST:event_btnNuevoViajeActionPerformed

    private void btnMisViajesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMisViajesActionPerformed
        // TODO add your handling code here:
        PopUp p= new PopUp();
        p.add(new pnlMisViajes(modelo, p));
        p.pack();
        p.setLocationRelativeTo(null);
        p.setVisible(true);
    }//GEN-LAST:event_btnMisViajesActionPerformed

    private void buscar() {
        if (txtBuscador.getText().length() <= 3) {
            JOptionPane.showMessageDialog(this, "Debe ingresar una palabra clave (mÃĄs de tres letras) en el cuadro de busqueda", "Busqueda VacÃ­a", JOptionPane.INFORMATION_MESSAGE);
        } else {
            padre.remove(this);
            padre.add(new pnlResultadoBusqueda(modelo, padre, txtBuscador.getText()), BorderLayout.WEST);
            padre.pack();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdministracion;
    private javax.swing.JButton btnMisViajes;
    private javax.swing.JButton btnNuevoViaje;
    private javax.swing.JLabel lblBuscador;
    private javax.swing.JLabel lblBuscar;
    private javax.swing.JLabel lblImagen;
    private javax.swing.JTextField txtBuscador;
    // End of variables declaration//GEN-END:variables
}