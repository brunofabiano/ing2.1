package DescansoApp.interfaz;

import DescansoApp.dominio.Sistema;
import java.awt.BorderLayout;
import javax.swing.JFrame;

public class pnlAdmin extends javax.swing.JPanel implements java.beans.Customizer {
    
    private Object bean;
    private final JFrame padre;
    private final Sistema modelo;

    public pnlAdmin(Sistema unModelo, JFrame miPadre) {
        modelo = unModelo;
        padre = miPadre;
        initComponents();
        btnAgregarCentro.setBackground(java.awt.Color.cyan);
        btnAgregarCiudad.setBackground(java.awt.Color.cyan);
        btnEditarComercios.setBackground(java.awt.Color.cyan);
        btnVerCiudades.setBackground(java.awt.Color.cyan);
        lblVolver.setBackground(java.awt.Color.cyan);
        this.setMinimumSize(new java.awt.Dimension(800, 600)); 
    }
    
    @Override
    public void setObject(Object bean) {
        this.bean = bean;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAgregarCiudad = new javax.swing.JButton();
        btnVerCiudades = new javax.swing.JButton();
        btnAgregarCentro = new javax.swing.JButton();
        btnEditarComercios = new javax.swing.JButton();
        lblVolver = new javax.swing.JLabel();
        lblImagen = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(840, 500));
        setLayout(null);

        btnAgregarCiudad.setText("Agregar Ciudad");
        btnAgregarCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarCiudadActionPerformed(evt);
            }
        });
        add(btnAgregarCiudad);
        btnAgregarCiudad.setBounds(40, 60, 160, 60);

        btnVerCiudades.setText("Ver Ciudades");
        btnVerCiudades.setMaximumSize(new java.awt.Dimension(107, 23));
        btnVerCiudades.setMinimumSize(new java.awt.Dimension(107, 23));
        btnVerCiudades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerCiudadesActionPerformed(evt);
            }
        });
        add(btnVerCiudades);
        btnVerCiudades.setBounds(40, 130, 160, 60);

        btnAgregarCentro.setText("Agregar Centro");
        btnAgregarCentro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarCentroActionPerformed(evt);
            }
        });
        add(btnAgregarCentro);
        btnAgregarCentro.setBounds(40, 200, 160, 60);

        btnEditarComercios.setText("Editar Comercios");
        btnEditarComercios.setMaximumSize(new java.awt.Dimension(107, 23));
        btnEditarComercios.setMinimumSize(new java.awt.Dimension(107, 23));
        btnEditarComercios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarComerciosActionPerformed(evt);
            }
        });
        add(btnEditarComercios);
        btnEditarComercios.setBounds(40, 270, 160, 60);

        lblVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/btnVolver.png"))); // NOI18N
        lblVolver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblVolverMouseClicked(evt);
            }
        });
        add(lblVolver);
        lblVolver.setBounds(40, 30, 26, 19);

        lblImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/FondoInicio.png"))); // NOI18N
        lblImagen.setPreferredSize(new java.awt.Dimension(800, 450));
        lblImagen.setVerifyInputWhenFocusTarget(false);
        add(lblImagen);
        lblImagen.setBounds(0, 0, 840, 500);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarCiudadActionPerformed
        padre.remove(this);
        padre.add(new pnlAgregarCiudad(modelo, null, padre), BorderLayout.WEST);
        padre.pack();
        
    }//GEN-LAST:event_btnAgregarCiudadActionPerformed

    private void btnVerCiudadesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerCiudadesActionPerformed
        padre.remove(this);
        padre.add(new pnlResultadoBusqueda(modelo, padre, "*"));
        padre.pack();
    }//GEN-LAST:event_btnVerCiudadesActionPerformed

    private void btnAgregarCentroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarCentroActionPerformed
        padre.remove(this);
        padre.add(new pnlAgregarComercio(modelo, padre, null));
        padre.pack();
    }//GEN-LAST:event_btnAgregarCentroActionPerformed

    private void btnEditarComerciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarComerciosActionPerformed
        padre.remove(this);
        padre.add(new pnlSeleccionarComercioEditar(modelo, padre));
        padre.pack();
    }//GEN-LAST:event_btnEditarComerciosActionPerformed

    private void lblVolverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblVolverMouseClicked
        // TODO add your handling code here:
        padre.remove(this);
        padre.add(new pnlInicio(modelo, padre));
        padre.pack();
    }//GEN-LAST:event_lblVolverMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarCentro;
    private javax.swing.JButton btnAgregarCiudad;
    private javax.swing.JButton btnEditarComercios;
    private javax.swing.JButton btnVerCiudades;
    private javax.swing.JLabel lblImagen;
    private javax.swing.JLabel lblVolver;
    // End of variables declaration//GEN-END:variables
}
