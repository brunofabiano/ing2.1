package DescansoApp.interfaz;

import DescansoApp.dominio.Ciudad;
import DescansoApp.dominio.ComercioActividad;
import DescansoApp.dominio.Sistema;
import DescansoApp.herramientas.TipoCA;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class pnlAgregarComercio extends javax.swing.JPanel implements java.beans.Customizer {
    
    private Object bean;
    private JFrame padre;
    private Sistema modelo;
    private ComercioActividad comercio;
    private ComercioActividad comercioAux;

    public pnlAgregarComercio(Sistema unModelo, JFrame miPadre, ComercioActividad unComercio ) {
        initComponents();
        modelo = unModelo;
        padre = miPadre;
        for(int i=comboTipoCentro.getItemCount()-1;i>=0;i--){
            comboTipoCentro.removeItemAt(i);
        }
        for(int i=comboCiudad.getItemCount()-1;i>=0;i--){
            comboCiudad.removeItemAt(i);
        }
        
        comboTipoCentro.addItem("Alojamiento");
        comboTipoCentro.addItem("Establecimiento Gastronomico");
        comboTipoCentro.addItem("Actividad");
        comboTipoCentro.addItem("Otro");
                
        for(Ciudad c : modelo.getListaCiudades()){
            comboCiudad.addItem(c.getNombre());
        }
        
        comercioAux = unComercio;
        if(comercioAux != null){
            textNombre.setText(comercioAux.getNombre());
            textNombre.setEditable(false);
            comboCiudad.setEditable(false);
            textCategoria.setText(comercioAux.getCategoria());
            textDetalles.setText(comercioAux.getDetalles());
            textHorario.setText(comercioAux.getHorario());
            textPrecio.setText(comercioAux.getPrecio());
            textTelefono.setText(comercioAux.getTelefono());
            textUbicacion.setText(comercioAux.getUbicacion());
            textWeb.setText(comercioAux.getWeb());
            
            String tipoSel = comercioAux.getTipo().toString();
            switch(tipoSel){
                case "alojamiento":
                    comboTipoCentro.setSelectedIndex(0);
                    break;
                case "estGastronomico":
                    comboTipoCentro.setSelectedIndex(1);
                    break;
                case "actividad":
                    comboTipoCentro.setSelectedIndex(2);
                    break;
                default:
                    comboTipoCentro.setSelectedIndex(3);
                    break;
            }
            
            //no se puede seleccionar la ciudad. habria que poner el campo ciudad en en objeto ComercioActividad
        }
        else {
            comercioAux = new ComercioActividad();
        }
            
    }
    
    @Override
    public void setObject(Object bean) {
        this.bean = bean;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        comboCiudad = new javax.swing.JComboBox<>();
        comboTipoCentro = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        textNombre = new javax.swing.JTextField();
        textTelefono = new javax.swing.JTextField();
        textHorario = new javax.swing.JTextField();
        textUbicacion = new javax.swing.JTextField();
        textCategoria = new javax.swing.JTextField();
        textPrecio = new javax.swing.JTextField();
        textWeb = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        textDetalles = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        textRutasImagenes = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        lblVolver = new javax.swing.JLabel();
        lblFondo = new javax.swing.JLabel();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(840, 460));
        setLayout(null);

        jLabel1.setText("Tipo Comercio");
        add(jLabel1);
        jLabel1.setBounds(40, 110, 190, 20);

        comboCiudad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCiudadActionPerformed(evt);
            }
        });
        add(comboCiudad);
        comboCiudad.setBounds(140, 60, 100, 30);

        comboTipoCentro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboTipoCentro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboTipoCentroActionPerformed(evt);
            }
        });
        add(comboTipoCentro);
        comboTipoCentro.setBounds(140, 110, 100, 30);

        jLabel2.setText("Ciudad:");
        add(jLabel2);
        jLabel2.setBounds(40, 70, 70, 14);

        textNombre.setText("nombre");
        textNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textNombreActionPerformed(evt);
            }
        });
        add(textNombre);
        textNombre.setBounds(320, 60, 90, 20);

        textTelefono.setText("telefono");
        textTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textTelefonoActionPerformed(evt);
            }
        });
        add(textTelefono);
        textTelefono.setBounds(320, 190, 90, 20);

        textHorario.setText("horario");
        textHorario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textHorarioActionPerformed(evt);
            }
        });
        add(textHorario);
        textHorario.setBounds(500, 120, 90, 20);

        textUbicacion.setText("ubicacion");
        textUbicacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textUbicacionActionPerformed(evt);
            }
        });
        add(textUbicacion);
        textUbicacion.setBounds(140, 190, 90, 20);

        textCategoria.setText("categoria");
        textCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textCategoriaActionPerformed(evt);
            }
        });
        add(textCategoria);
        textCategoria.setBounds(500, 60, 90, 20);

        textPrecio.setText("precio");
        textPrecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textPrecioActionPerformed(evt);
            }
        });
        add(textPrecio);
        textPrecio.setBounds(320, 120, 90, 20);

        textWeb.setText("web");
        textWeb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textWebActionPerformed(evt);
            }
        });
        add(textWeb);
        textWeb.setBounds(500, 190, 90, 20);

        jButton1.setText("Agregar Imagen");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        add(jButton1);
        jButton1.setBounds(340, 300, 140, 40);

        textDetalles.setColumns(20);
        textDetalles.setRows(5);
        textDetalles.setText("detalles\n");
        jScrollPane1.setViewportView(textDetalles);

        add(jScrollPane1);
        jScrollPane1.setBounds(140, 270, 166, 96);

        textRutasImagenes.setColumns(20);
        textRutasImagenes.setRows(5);
        textRutasImagenes.setText("\n");
        jScrollPane2.setViewportView(textRutasImagenes);

        add(jScrollPane2);
        jScrollPane2.setBounds(520, 270, 166, 96);

        jButton2.setText("Guardar Comercio");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        add(jButton2);
        jButton2.setBounds(340, 410, 140, 40);

        lblVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/btnVolver.png"))); // NOI18N
        lblVolver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblVolverMouseClicked(evt);
            }
        });
        add(lblVolver);
        lblVolver.setBounds(44, 21, 50, 30);

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/InformacionCiudad.png"))); // NOI18N
        lblFondo.setDoubleBuffered(true);
        lblFondo.setPreferredSize(new java.awt.Dimension(840, 500));
        add(lblFondo);
        lblFondo.setBounds(0, 0, 880, 470);
    }// </editor-fold>//GEN-END:initComponents

    private void comboCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCiudadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboCiudadActionPerformed

    private void comboTipoCentroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboTipoCentroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboTipoCentroActionPerformed

    private void textNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textNombreActionPerformed

    private void textTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textTelefonoActionPerformed

    private void textHorarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textHorarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textHorarioActionPerformed

    private void textUbicacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textUbicacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textUbicacionActionPerformed

    private void textCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textCategoriaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textCategoriaActionPerformed

    private void textPrecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textPrecioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textPrecioActionPerformed

    private void textWebActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textWebActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textWebActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(textNombre.getText().equals("")){
            JOptionPane optionPane = new JOptionPane("Ingrese un nombre antes de cargar las imágenes",JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Error!");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true);
        }else{
            JOptionPane optionPane = new JOptionPane("No cambie el directorio. Seleccione una imagen del directorio por defecto.",JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Atencion");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true);
            
            jFileChooser1.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\src\\DescansoApp\\baseDatos"));
            int returnVal = jFileChooser1.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = jFileChooser1.getSelectedFile();
                
                comercio = new ComercioActividad();
                
                String inicioRuta = "/DescansoApp/baseDatos/";
                        
                for(ImageIcon i: comercioAux.getImagenes()){
                    String[] nombre = i.getDescription().split("/");
                    String name = nombre[nombre.length-1];
                    comercio.agregarImagen(inicioRuta + name);
                }
                
                String ruta = inicioRuta + file.getName();
                comercio.agregarImagen(ruta);
                try {   
                    textRutasImagenes.append(file.getAbsolutePath() + "\n");
                } catch (Exception ex) {
                  System.out.println("Ocurrio un problema con el archivo"+file.getAbsolutePath());
                }
            } else {
                System.out.println("Cancelado.");
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Ciudad ciudadSeleccionada = modelo.buscarCiudadPorNombre(comboCiudad.getSelectedItem().toString());
        
        if(comercio == null){
            comercio = new ComercioActividad();
            for(ImageIcon i: comercioAux.getImagenes()){
                comercioAux.agregarImagen(i.getDescription());
            }
        }
        
        comercio.setNombre(textNombre.getText());
        comercio.setCategoria(textCategoria.getText());
        comercio.setDetalles(textDetalles.getText());
        comercio.setHorario(textHorario.getText());
        comercio.setPrecio(textPrecio.getText());
        comercio.setTelefono(textTelefono.getText());
        comercio.setUbicacion(textUbicacion.getText());
        comercio.setWeb(textWeb.getText());
        
        TipoCA tipo;
        switch(comboTipoCentro.getSelectedItem().toString()){
            case "Alojamiento":
                tipo = TipoCA.alojamiento;
                break;
            case "Actividad":
                tipo = TipoCA.actividad;
                break;
            case "Establecimiento Gastronomico":
                tipo = TipoCA.estGastronomico;
                break;
            default:
                tipo = TipoCA.otros;
                break;
        }
        comercio.setTipo(tipo);
        
        modelo.borrarComercioPorNombreYCiudad(textNombre.getText(), ciudadSeleccionada);
        ciudadSeleccionada.agregarComercioActividad(tipo, comercio);
               
        JOptionPane optionPane = new JOptionPane("El comercio ha sido ingresado en el sistema",JOptionPane.WARNING_MESSAGE);
        JDialog dialog = optionPane.createDialog("Comercio agregado");
        dialog.setAlwaysOnTop(true); // to show top of all other application
        dialog.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void lblVolverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblVolverMouseClicked
        // TODO add your handling code here:
        padre.remove(this);
        padre.add(new pnlAdmin(modelo, padre));
        padre.pack();
    }//GEN-LAST:event_lblVolverMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboCiudad;
    private javax.swing.JComboBox<String> comboTipoCentro;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblVolver;
    private javax.swing.JTextField textCategoria;
    private javax.swing.JTextArea textDetalles;
    private javax.swing.JTextField textHorario;
    private javax.swing.JTextField textNombre;
    private javax.swing.JTextField textPrecio;
    private javax.swing.JTextArea textRutasImagenes;
    private javax.swing.JTextField textTelefono;
    private javax.swing.JTextField textUbicacion;
    private javax.swing.JTextField textWeb;
    // End of variables declaration//GEN-END:variables
}
