package DescansoApp.interfaz;

import DescansoApp.dominio.Ciudad;
import DescansoApp.dominio.Sistema;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class pnlAgregarCiudad extends javax.swing.JPanel implements java.beans.Customizer {
    
    private final Sistema modelo;
    private Ciudad ciudad;
    private Object bean;
    private Ciudad ciudadAux;
    private final JFrame padre;

    public pnlAgregarCiudad(Sistema unModelo, Ciudad unaCiudad, JFrame miPadre) {
        modelo = unModelo;
        padre = miPadre;
        initComponents();
        if (unaCiudad != null) {
            //Busco la ciudad para cargar sus valores en los campos
            ciudadAux = unModelo.buscarCiudadPorNombre(unaCiudad.getNombre());
            if (ciudadAux != null){
                this.jTextNombre.setText(ciudadAux.getNombre());
                this.textDescrip.setText(ciudadAux.getDescripcion());
                this.textInfoGral.setText(ciudadAux.getInfoGral());
            }
        }
        else {
            ciudadAux = new Ciudad();
        }
    }
    
    @Override
    public void setObject(Object bean) {
        this.bean = bean;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        textInfoGral = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        textDescrip = new javax.swing.JTextArea();
        btnAgregarImagen = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        textRutasImagenes = new javax.swing.JTextArea();
        btnGuardarCiudad = new javax.swing.JButton();
        lblVolver = new javax.swing.JLabel();
        lblFondo = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(840, 500));
        setLayout(null);

        jLabel1.setText("Descripción:");
        add(jLabel1);
        jLabel1.setBounds(70, 80, 70, 14);

        jLabel2.setText("Info gral:");
        add(jLabel2);
        jLabel2.setBounds(470, 80, 70, 14);

        jLabel3.setText("Imagenes:");
        add(jLabel3);
        jLabel3.setBounds(70, 220, 80, 14);

        jLabel4.setText("Nombre:");
        add(jLabel4);
        jLabel4.setBounds(160, 40, 50, 14);

        jTextNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextNombreActionPerformed(evt);
            }
        });
        add(jTextNombre);
        jTextNombre.setBounds(210, 40, 330, 20);

        textInfoGral.setColumns(20);
        textInfoGral.setRows(5);
        jScrollPane1.setViewportView(textInfoGral);

        add(jScrollPane1);
        jScrollPane1.setBounds(460, 100, 310, 96);

        textDescrip.setColumns(20);
        textDescrip.setRows(5);
        jScrollPane2.setViewportView(textDescrip);

        add(jScrollPane2);
        jScrollPane2.setBounds(60, 100, 310, 96);

        btnAgregarImagen.setText("Agregar");
        btnAgregarImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarImagenActionPerformed(evt);
            }
        });
        add(btnAgregarImagen);
        btnAgregarImagen.setBounds(160, 360, 110, 23);

        textRutasImagenes.setColumns(20);
        textRutasImagenes.setRows(5);
        jScrollPane3.setViewportView(textRutasImagenes);

        add(jScrollPane3);
        jScrollPane3.setBounds(60, 240, 310, 100);

        btnGuardarCiudad.setText("Guardar Ciudad");
        btnGuardarCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarCiudadActionPerformed(evt);
            }
        });
        add(btnGuardarCiudad);
        btnGuardarCiudad.setBounds(630, 420, 170, 50);

        lblVolver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/btnVolver.png"))); // NOI18N
        lblVolver.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblVolverMouseClicked(evt);
            }
        });
        add(lblVolver);
        lblVolver.setBounds(70, 40, 26, 19);

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/InformacionCiudad.png"))); // NOI18N
        lblFondo.setPreferredSize(new java.awt.Dimension(840, 500));
        add(lblFondo);
        lblFondo.setBounds(0, 0, 840, 500);
    }// </editor-fold>//GEN-END:initComponents

    private void jTextNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextNombreActionPerformed

    private void btnAgregarImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarImagenActionPerformed
        if(jTextNombre.getText().equals("")){
            JOptionPane optionPane = new JOptionPane("Ingrese un nombre antes de cargar las imágenes",JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Error!");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true);
        }else{
            JOptionPane optionPane = new JOptionPane("No cambie el directorio. Seleccione una imagen del directorio por defecto.",JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Atencion");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true);
            
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")+"\\src\\DescansoApp\\baseDatos"));
            int returnVal = fileChooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                
                String inicioRuta = "/DescansoApp/baseDatos/";
                ciudad = new Ciudad();
                
                if(ciudadAux != null){
                    //Estoy modificando una ciudad
                    for(ImageIcon i: ciudadAux.getImagenes()){
                        String[] nombre = i.getDescription().split("/");
                        String name = nombre[nombre.length-1];
                        ciudad.agregarImagen(inicioRuta + name);
                    }
                }
                else
                {
                    for(ImageIcon i: ciudadAux.getImagenes()){
                        String[] nombre = i.getDescription().split("/");
                        String name = nombre[nombre.length-1];
                        ciudad.agregarImagen(inicioRuta + name);
                    }    
                }
                
                String ruta = inicioRuta + file.getName();
                ciudad.agregarImagen(ruta);
                try {   
                    textRutasImagenes.append(file.getAbsolutePath() + "\n");
                } catch (Exception ex) {
                  System.out.println("Ocurrio un problema con el archivo"+file.getAbsolutePath());
                }
            } else {
                System.out.println("Cancelado.");
            }
        }
    }//GEN-LAST:event_btnAgregarImagenActionPerformed

    private void btnGuardarCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarCiudadActionPerformed
        if(ciudad == null){
            ciudad = new Ciudad();
            if (ciudad.getImagenes().size()>0){
                for(ImageIcon i: ciudadAux.getImagenes()){
                    ciudad.agregarImagen(i.getDescription());
                }
            }
            
        }
        if(jTextNombre.getText().equals("") || textDescrip.getText().equals("") || textInfoGral.getText().equals("")){
            JOptionPane optionPane = new JOptionPane("Debe ingresar toda la informacion en los campos",JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Atencion");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true);
        }
        else {
            ciudad.setNombre(jTextNombre.getText());
            ciudad.setDescripcion(textDescrip.getText());
            ciudad.setInfoGral(textInfoGral.getText());
        
            modelo.borrarCiudad(jTextNombre.getText());
            modelo.agregarCiudad(ciudad);
        
            JOptionPane optionPane = new JOptionPane("La ciudad ha sido ingresada en el sistema",JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Ciudad agregada");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true);
        }
        
    }//GEN-LAST:event_btnGuardarCiudadActionPerformed

    private void lblVolverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblVolverMouseClicked
        // TODO add your handling code here:
        padre.remove(this);
        padre.add(new pnlAdmin(modelo, padre));
        padre.pack();
    }//GEN-LAST:event_lblVolverMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarImagen;
    private javax.swing.JButton btnGuardarCiudad;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextNombre;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblVolver;
    private javax.swing.JTextArea textDescrip;
    private javax.swing.JTextArea textInfoGral;
    private javax.swing.JTextArea textRutasImagenes;
    // End of variables declaration//GEN-END:variables
}
